package com.example.ex1_contacts;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    static ArrayList<String> contact_list = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        String ln = getIntent().getExtras().getString("lastname");
        String fn = getIntent().getExtras().getString("firstname");
        String tel = getIntent().getExtras().getString("phone");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contact_list);
        ListView lv = findViewById(R.id.contactList);

        assert ln != null;
        assert fn != null;
        assert tel != null;
        if ((!ln.equals("")) && (!fn.equals("")) && (!tel.equals(""))) {
            String val = ln.concat(" ").concat(fn).concat(" : ").concat(tel);
            contact_list.add(val);
            //adapter.add(val);
            writeToFile(val, this);
        }

        lv.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(ListActivity.this, MainActivity.class);
                //startActivity(intent);
                finish();
            }
        });

        ((Button) findViewById(R.id.buttonFile)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        readFromFile(ListActivity.this);
                    }
                });

    }

    private void writeToFile(String data,Context context) {

        // Ecriture
        try {
            //Toast.makeText(this, "POGGIES", Toast.LENGTH_SHORT).show();

            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("contacts.txt", Context.MODE_APPEND));
            outputStreamWriter.append(data);
            outputStreamWriter.append("\n");
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }

    }

    private void readFromFile(Context context) {

        // Lecture
        String ret = "";
        try {
            InputStream inputStream = context.openFileInput("contacts.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append("\n").append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        //Toast.makeText(this, ret, Toast.LENGTH_SHORT).show();
        TextView tv2 = findViewById(R.id.fileContent);
        tv2.setText(ret);

    }

}
