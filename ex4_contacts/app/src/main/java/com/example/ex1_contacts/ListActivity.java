package com.example.ex1_contacts;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    private ArrayList<String> contact_list = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contact_list);
        ListView lv = findViewById(R.id.contactList);

        final MaBaseOpenHelper contactsBdd = new MaBaseOpenHelper(ListActivity.this, "contacts.db",null,1);
        final SQLiteDatabase my_db = contactsBdd.getWritableDatabase();

        displayData(my_db);

        lv.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(ListActivity.this, MainActivity.class);
                //startActivity(intent);
                finish();
            }
        });
    }

    private void displayData(SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("SELECT * FROM  table_contacts",null);
        contact_list.clear();
        if (cursor.moveToFirst()) {
            do {
                String cnom = cursor.getString(cursor.getColumnIndex("nom"));
                String cprenom = cursor.getString(cursor.getColumnIndex("prenom"));
                String ctel = cursor.getString(cursor.getColumnIndex("tel"));
                String contact_concatene = cnom.concat(" ").concat(cprenom).concat(" : ").concat(ctel);
                contact_list.add(contact_concatene);
            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    //private void writeToFile(String data,Context context) {
//
    //    // Ecriture
    //    try {
    //        //Toast.makeText(this, "POGGIES", Toast.LENGTH_SHORT).show();
//
    //        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("contacts.txt", Context.MODE_APPEND));
    //        outputStreamWriter.append(data);
    //        outputStreamWriter.append("\n");
    //        outputStreamWriter.close();
    //    }
    //    catch (IOException e) {
    //        Log.e("Exception", "File write failed: " + e.toString());
    //    }
//
    //}

    //private void readFromFile(Context context) {
//
    //    // Lecture
    //    String ret = "";
    //    try {
    //        InputStream inputStream = context.openFileInput("contacts.txt");
//
    //        if ( inputStream != null ) {
    //            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
    //            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
    //            String receiveString = "";
    //            StringBuilder stringBuilder = new StringBuilder();
//
    //            while ( (receiveString = bufferedReader.readLine()) != null ) {
    //                stringBuilder.append("\n").append(receiveString);
    //            }
//
    //            inputStream.close();
    //            ret = stringBuilder.toString();
    //        }
    //    }
    //    catch (FileNotFoundException e) {
    //        Log.e("login activity", "File not found: " + e.toString());
    //    } catch (IOException e) {
    //        Log.e("login activity", "Can not read file: " + e.toString());
    //    }
//
    //    //Toast.makeText(this, ret, Toast.LENGTH_SHORT).show();
    //    TextView tv2 = findViewById(R.id.fileContent);
    //    tv2.setText(ret);
//
    //}

}
