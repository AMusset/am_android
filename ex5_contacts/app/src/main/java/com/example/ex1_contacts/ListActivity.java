package com.example.ex1_contacts;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListActivity extends AppCompatActivity {

    private ArrayList<String> contact_list = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        String ln = getIntent().getExtras().getString("lastname");
        String fn = getIntent().getExtras().getString("firstname");
        String tel = getIntent().getExtras().getString("phone");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contact_list);
        ListView lv = findViewById(R.id.contactList);

        final MaBaseOpenHelper contactsBdd = new MaBaseOpenHelper(ListActivity.this, "contacts.db",null,1);
        final SQLiteDatabase my_db = contactsBdd.getWritableDatabase();

        displayData(my_db);

        assert ln != null;
        assert fn != null;
        assert tel != null;
        if ((!ln.equals("")) && (!fn.equals("")) && (!tel.equals(""))) {
            String val = ln.concat(" ").concat(fn).concat(" : ").concat(tel);
            contact_list.add(val);
            //adapter.add(val);
            writeToFile(val, this);
        }

        lv.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ((Button) findViewById(R.id.buttonFile)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        readFromFile(ListActivity.this);
                    }
                });

        ((Button) findViewById(R.id.buttonCheck)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        compareDbFile(my_db, ListActivity.this);
                    }
                });
    }

    private void displayData(SQLiteDatabase db) {
        Cursor cursor = db.rawQuery("SELECT * FROM  table_contacts",null);
        contact_list.clear();
        if (cursor.moveToFirst()) {
            do {
                String cnom = cursor.getString(cursor.getColumnIndex("nom"));
                String cprenom = cursor.getString(cursor.getColumnIndex("prenom"));
                String ctel = cursor.getString(cursor.getColumnIndex("tel"));
                String contact_concatene = cnom.concat(" ").concat(cprenom).concat(" : ").concat(ctel);
                contact_list.add(contact_concatene);
            } while (cursor.moveToNext());
        }

        cursor.close();
    }

    private void writeToFile(String data,Context context) {

        // Ecriture
        try {
            //Toast.makeText(this, "POGGIES", Toast.LENGTH_SHORT).show();

            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("contacts.txt", Context.MODE_APPEND));
            outputStreamWriter.append(data);
            outputStreamWriter.append("\n");
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }

    }

    private String readFromFile(Context context) {

        // Lecture
        String ret = "";
        try {
            InputStream inputStream = context.openFileInput("contacts.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append("\n").append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;

        //Toast.makeText(this, ret, Toast.LENGTH_SHORT).show();
        //TextView tv2 = findViewById(R.id.fileContent);
        //tv2.setText(ret);

    }

    private void compareDbFile(SQLiteDatabase db, Context context) {

        String strFromFile = readFromFile(context);

        ///

        Cursor cursor = db.rawQuery("SELECT * FROM  table_contacts",null);
        contact_list.clear();
        StringBuilder strFromDb = new StringBuilder();
        if (cursor.moveToFirst()) {
            do {
                String cnom = cursor.getString(cursor.getColumnIndex("nom"));
                String cprenom = cursor.getString(cursor.getColumnIndex("prenom"));
                String ctel = cursor.getString(cursor.getColumnIndex("tel"));
                String contact_concatene = cnom.concat(" ").concat(cprenom).concat(" : ").concat(ctel);
                strFromDb.append("\n").append(contact_concatene);
            } while (cursor.moveToNext());
        }

        String trueStrFromDb = strFromDb.toString();
        cursor.close();

        if ( trueStrFromDb.equals(strFromFile) ) {
            Toast.makeText(this, "Les données correspondent", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Les données ne correspondent pas, màj en cours...", Toast.LENGTH_LONG).show();
            // Traitement
            List<String> itemsDb = Arrays.asList(trueStrFromDb.split("\n"));
            List<String> itemsFile = Arrays.asList(strFromFile.split("\n"));
            for (int i = 0; i<itemsFile.size(); i++ ) {
                boolean boolEqual = false;

                for (int a = 0; a<itemsDb.size(); a++ ) {
                    if (itemsFile.get(i).equals(itemsDb.get(a))) {
                        boolEqual = true;
                    }
                }

                if (boolEqual = false) {
                    //Ajouter itemsFile.get(i) dans la db
                    List<String> strToDb = Arrays.asList(strFromFile.split(" "));
                    final MaBaseOpenHelper contactsBdd = new MaBaseOpenHelper(ListActivity.this, "contacts.db",null,1);
                    final SQLiteDatabase my_db = contactsBdd.getWritableDatabase();
                    contactsBdd.setNewContact(my_db, strToDb.get(0), strToDb.get(1), Integer.parseInt(strToDb.get(2)));
                }

            }
        }
    }

}
