package com.example.ex1_contacts;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MaBaseOpenHelper extends SQLiteOpenHelper {

    private static final int BASE_VERSION = 1;
    private static final String BASE_NOM = "contacts.db";
    private static final String TABLE_CONTACTS = "table_contacts";

    public static final String COLONNE_ID = "id";
    public static final int COLONNE_ID_ID = 0;
    public static final String COLONNE_NOM = "nom";
    public static final int COLONNE_NOM_ID = 1;
    public static final String COLONNE_PRENOM = "prenom";
    public static final int COLONNE_PRENOM_ID = 2;
    public static final String COLONNE_TEL = "tel";
    public static final int COLONNE_TEL_ID = 2;

    private static final String REQUETE_CREATION_BD = "create table " + TABLE_CONTACTS
            + " (" + COLONNE_ID + " integer primary key autoincrement, "
            + COLONNE_NOM + " text not null, "
            + COLONNE_PRENOM + " text not null, "
            + COLONNE_TEL + " integer not null);";

    public SQLiteDatabase maBaseDonnees;

    public MaBaseOpenHelper(Context context, String nom, SQLiteDatabase.CursorFactory cursorfactory, int version) {
        super(context, nom, cursorfactory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(REQUETE_CREATION_BD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table TABLE_CONTACTS ;");
        onCreate(db);
    }

    public void setNewContact(SQLiteDatabase db, String nomContact, String prenomContact, int telContact) {
        db.execSQL("insert into table_contacts (nom,prenom,tel) " + "values ('" + nomContact + "','" + prenomContact + "','" + telContact + "');");
    }

    //public void getAllContacts(SQLiteDatabase db) {
    //    Cursor c = db.query();
    //}


}
