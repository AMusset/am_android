package com.example.ex1_contacts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    static int count = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final MaBaseOpenHelper contactsBdd = new MaBaseOpenHelper(MainActivity.this, "contacts.db",null,1);
        final SQLiteDatabase my_db = contactsBdd.getWritableDatabase();

        TextView tv = findViewById(R.id.textCount);
        tv.setText(String.valueOf(count));

        //Intent intent = new Intent(MainActivity.this, MainActivity.class);
        //startActivityForResult(intent, 1);

        // Vers la liste de contact sans ajouter l'actuel
        ((Button) findViewById(R.id.buttonList)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, ListActivity.class);
                        intent.putExtra("lastname", "");
                        intent.putExtra("firstname", "");
                        intent.putExtra("phone", "");
                        startActivity(intent);
                    }
                });

        // Vers la liste de contacts en ajoutant l'actuel saisi
        ((Button) findViewById(R.id.buttonVal)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, ListActivity.class);

                        EditText etln = findViewById(R.id.editTextLn);
                        EditText etfn = findViewById(R.id.editTextFn);
                        EditText ettel = findViewById(R.id.editTextTel);
                        String contentEtln = etln.getText().toString();
                        String contentEtfn = etfn.getText().toString();
                        String contentEttel = ettel.getText().toString();
                        int contentEttel2 = Integer.parseInt(contentEttel);
                        // Creation nouv contact
                        if ((!contentEtln.equals("")) && (!contentEtfn.equals("")) && (!contentEttel.equals(""))) {
                            contactsBdd.setNewContact(my_db, contentEtln, contentEtfn, contentEttel2);
                        }
                        intent.putExtra("lastname", contentEtln);
                        intent.putExtra("firstname", contentEtfn);
                        intent.putExtra("phone", contentEttel);
                        startActivity(intent);
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //count += 1;
        //TextView tv = findViewById(R.id.textCount);
        //tv.setText(String.valueOf(count));
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Toast.makeText(this, "Etat de l'activité sauvegardé", Toast.LENGTH_SHORT).show();
        count += 1;
        TextView tv = findViewById(R.id.textCount);
        tv.setText(String.valueOf(count));
    }

    //public void onClick(View v) {
    //    switch(v.getId()){
    //        case R.id.buttonVal:
    //            setResult(RESULT_OK);
    //            finish();
    //            break;
    //        case R.id.buttonList:
    //            setResult(RESULT_CANCELED);
    //            finish();
    //            break;
    //    }
    //}

    //protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    //    super.onActivityResult(requestCode, resultCode, data);
    //    if (requestCode == 1) {
    //        switch (resultCode) {
    //            case RESULT_OK:
    //                Toast.makeText(this, "Action validée", Toast.LENGTH_LONG).show();
    //                break;
    //            case RESULT_CANCELED:
    //                Toast.makeText(this, "Action annulée", Toast.LENGTH_LONG).show();
    //                break;
    //            default:
    //                Toast.makeText(this, "Oula y'a eu qqch de bizarre là", Toast.LENGTH_LONG).show();
    //                break;
    //        }
    //    }
    //}
}
