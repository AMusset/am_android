package com.example.ex1_contacts;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    static ArrayList<String> contact_list = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        String ln = getIntent().getExtras().getString("lastname");
        String fn = getIntent().getExtras().getString("firstname");
        String tel = getIntent().getExtras().getString("phone");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, contact_list);
        ListView lv = findViewById(R.id.contactList);

        assert ln != null;
        assert fn != null;
        assert tel != null;
        if ((!ln.equals("")) && (!fn.equals("")) && (!tel.equals(""))) {
            String val = ln.concat(" ").concat(fn).concat(" : ").concat(tel);
            contact_list.add(val);
            //adapter.add(val);
        }

        lv.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(ListActivity.this, MainActivity.class);
                //startActivity(intent);
                finish();
            }
        });
    }

}
