Résultats d'Alexis Musset, classe B2DEV aux exercices de la semaine de Mobile Application Development.
Commentaires :
- Au cours des exercices, il a été réfléchi d'utiliser une classe Contact basique incluant les deux string et l'int. 
Il a été convenu que l'on conserve la version avec un simple String concaténé pour la suite même s'il ne s'agit pas de la manière la plus propre de le faire.
- Exercice 2 : L'utilisation des outils d'enregistrement onSaveInstanceState() n'est peut-être pas exploité au potentiel attendu dans le cours mais la réponse est là et fonctionne.