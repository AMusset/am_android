package com.example.ex1_contacts;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Intent intent = new Intent(MainActivity.this, MainActivity.class);
        //startActivityForResult(intent, 1);

        // Vers la liste de contact sans ajouter l'actuel
        ((Button) findViewById(R.id.buttonList)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, ListActivity.class);
                        intent.putExtra("lastname", "");
                        intent.putExtra("firstname", "");
                        intent.putExtra("phone", "");
                        startActivity(intent);
                    }
                });

        // Vers la liste de contacts en ajoutant l'actuel saisi
        ((Button) findViewById(R.id.buttonVal)).
                setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, ListActivity.class);
                        EditText etln = findViewById(R.id.editTextLn);
                        EditText etfn = findViewById(R.id.editTextFn);
                        EditText ettel = findViewById(R.id.editTextTel);
                        String contentEtln = etln.getText().toString();
                        String contentEtfn = etfn.getText().toString();
                        String contentEttel = ettel.getText().toString();
                        intent.putExtra("lastname", contentEtln);
                        intent.putExtra("firstname", contentEtfn);
                        intent.putExtra("phone", contentEttel);
                        startActivity(intent);
                    }
                });
    }

    //public void onClick(View v) {
    //    switch(v.getId()){
    //        case R.id.buttonVal:
    //            setResult(RESULT_OK);
    //            finish();
    //            break;
    //        case R.id.buttonList:
    //            setResult(RESULT_CANCELED);
    //            finish();
    //            break;
    //    }
    //}

    //protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
    //    super.onActivityResult(requestCode, resultCode, data);
    //    if (requestCode == 1) {
    //        switch (resultCode) {
    //            case RESULT_OK:
    //                Toast.makeText(this, "Action validée", Toast.LENGTH_LONG).show();
    //                break;
    //            case RESULT_CANCELED:
    //                Toast.makeText(this, "Action annulée", Toast.LENGTH_LONG).show();
    //                break;
    //            default:
    //                Toast.makeText(this, "Oula y'a eu qqch de bizarre là", Toast.LENGTH_LONG).show();
    //                break;
    //        }
    //    }
    //}
}
